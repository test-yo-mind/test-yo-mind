package ru.podkovyrov.testyomind

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TestYoMindApplication

fun main(args: Array<String>) {
    runApplication<TestYoMindApplication>(*args)
}
