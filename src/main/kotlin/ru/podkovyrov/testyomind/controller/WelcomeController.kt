package ru.podkovyrov.testyomind.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class WelcomeController {

    @GetMapping("/pulse")
    fun getPulse(): String {
        return "I'm alive !0_0!"
    }

}